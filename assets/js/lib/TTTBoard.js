const 	EMPTY = 1,
		PLAYERX = 2,
		PLAYERO = 3,
		DRAW = 4,
		STAMP = new Map([[EMPTY, " "], [PLAYERX, "X"], [PLAYERO, "O"]])
;

module.exports = {
	EMPTY: EMPTY,
	PLAYERX: PLAYERX,
	PLAYERO: PLAYERO,
	DRAW: DRAW,
	TTTBoard: class TTTBoard {
		constructor(dimension, grid) {
			// initialize the TTTBoard with given dimension
			// or with giver grid
			this._dim = dimension;
			if (!grid)
				this._grid  = Array.from({length:3}, () => new Array(3).fill(EMPTY));
			else {
				this._grid = [];
				this.fillGrid(dimension, grid);
			}
		}
		toString(){
			// human readable representation of board
			let str = "\n";
			for( let i = 0; i < this._dim; i++ ) {
				for( let j = 0; j < this._dim; j++ ) {
					str += STAMP.get(this._grid[i][j]);
					if(j === (this._dim - 1)) {
						str += "\n";
					} else {
						str += "|";
					}
				}
			}

			return str;
		}
		get grid(){
			//getter return board grid
			return this._grid;
		}
		square(row, col) {
			// returns one of the three constants PLAYERX, PLAYERO, EMPTY
			// that correspond to the content of the grid at position [row, col]
			return this._grid[row][col];
		}
		fillGrid(dim, grid){
			// fill the board grid when the grid is given
			for( let i = 0; i < dim; i++ ) {
				this._grid[i] = [];
				for( let j = 0; j < dim; j++ ) {
					this._grid[i][j] = grid[i][j];
				}
			}
		}
		move(row, col, player){
			// place player on position [row,col]
			// player should be either the constant PLAYERX or PLAYERO
			// does nothing if the square is not EMPTY
			if (this._grid[row][col] === EMPTY)
				this._grid[row][col] = player;
		}
		getEmptySquares(){
			let empty = [];
			for (let i = 0; i < this._dim; i++){
				for (let j = 0; j < this._dim; j++) {
					if (this._grid[i][j] === EMPTY) {
						let square = [i,j];
						empty.push(square);
					}
				}
			}
			return empty;
		}
		checkWinner(){
			let grid = [],
				extend = Function.prototype.apply.bind(Array.prototype.push),
				cols = [],
				diags = [[],[]]
			;
			// rows
			extend(grid, this._grid);

			// cols
			for (let i = 0; i < this._dim; i++) {
				cols[i] = [];
				for(let j = 0; j < this._dim; j++) {
					cols[i][j] = this._grid[j][i];
				}
			}
			extend(grid, cols);

			// diags
			for (let i = 0; i < this._dim; i++) {
				diags[0][i] = this._grid[i][i];
				diags[1][i] = this._grid[i][this._dim - 1 - i];
			}
			extend(grid, diags);

			for (let i = 0; i < grid.length; i++){
				let set = new Set(grid[i]);
				if( set.size === 1 && grid[i][0] !== EMPTY ) {
					return grid[i][0];
				}
			}

			if (this.getEmptySquares().length === 0) {
				return DRAW;
			}

			// game is still in progress
			return null;
		}
		clone(){
			return new TTTBoard(this._dim, this._grid);
		}
	},
	switchPlayer: function(player){
		if (player === PLAYERO) {
			return PLAYERX;
		} else {
			return PLAYERO;
		}
	}
};