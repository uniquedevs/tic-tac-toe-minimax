const 	TTTBoardModule = require('./TTTBoard'),
		TTTBoard = TTTBoardModule.TTTBoard,
		switchPlayer = TTTBoardModule.switchPlayer,
		EMPTY = TTTBoardModule.EMPTY,
		PLAYERX = TTTBoardModule.PLAYERX,
		PLAYERO = TTTBoardModule.PLAYERO,
		DRAW = TTTBoardModule.DRAW,
		STAMP = new Map([[PLAYERX, "Player X"], [PLAYERO, "Player O"], [DRAW, "DRAW"]]),
		SCORE = new Map([[PLAYERX,1],[PLAYERO, -1]])
;

module.exports = {
	TTTGame: class TTTGame {
		constructor(dim, grid){
			this._board = new TTTBoard(dim, grid);
		}
		get board(){
			return this._board;
		}
		start(){
			let winner = null,
				current_player = PLAYERX
			;
			while (!winner) {
				let {square} = this.move( this._board, current_player ),
					row = square[0],
					col = square[1]
				;
				this._board.move(row, col, current_player);

				winner = this._board.checkWinner();

				if ( winner ) {
					console.log( this._board.toString() );
					console.log( STAMP.get(winner), ' win');
				} else {
					current_player = TTTBoardModule.switchPlayer(current_player);
				}
			}
			//console.log(this._board.toString());
			return winner;
		}
		randomMove(){
			// move method just for testing
			let empty = this._board.getEmptySquares();
			return {
				score: 1,
				square: empty[Math.floor(Math.random() * empty.length)]
			};
	 	}
		move(board, player, level){
			// minimax strategy machine player move
			let sum_scores = 0,
				best_score = -1,
				best_move,
				empties = board.getEmptySquares(),
				winner = board.checkWinner(),
				min_or_max = SCORE.get(player)

			;
			level = level || 1;
			//console.log('move for ', STAMP.get(player));
			//console.log('empties', empties);
			//console.log(winner);
			//console.log('level', level);

			if (winner) {
				//console.log( STAMP.get(winner), ' win');
				//onsole.log('board', board.toString());
				//console.log({
				//	score: winner === PLAYERX ? 1 : ( winner === PLAYERO ? -1 : 0 ),
				//	square: [-1,-1]
				//});
				//console.log('-------------------------------------------------------');
				return {
					score: winner === PLAYERX ? 1 : ( winner === PLAYERO ? -1 : 0 ),
					square: [-1,-1]
				};
			}

			for (let i = 0; i < empties.length; i++) {
				let new_board = board.clone()
				;

                new_board.move(empties[i][0], empties[i][1], player);
                //console.log(new_board.toString());

				let {score} = this.move(new_board, switchPlayer(player), level+1);

				sum_scores += score;

				if (score * min_or_max > best_score) {
					best_score = score;
					best_move = empties[i];
				}
			}


			//console.log(STAMP.get(player), ' move info and best score');
			//console.log({
            //    score: sum_scores,
            //    square: best_move
            //});
            //console.log('================================================================');


			return {
				score: sum_scores,
                square: best_move
			};
		}
		move_alternate(board, player){
			let empties = board.getEmptySquares(),
				scores = [],
				winner,
				min_or_max = SCORE.get(player)
			;
			for (let i = 0; i < empties.length; i++) {
				let new_board = board.clone(),
					cell = empties[i],
					result
				;
				new_board.move(cell[0], cell[1], player);
				result = this.move(new_board, switchPlayer(player));
				scores.push(result, cell);
			}
			winner = board.checkWinner();
			if (winner) {
				return {
					score: winner === PLAYERX ? 1 : ( winner === PLAYERO ? -1 : 0 ),
					square: [-1,-1]
				}
			} else {
				let best_square = scores[0].square,
					best_score = scores[0].score * min_or_max,
					sum_score = 0
				;
				best_score = scores.reduce( (max, current_score) => {
					sum_score += current_score.score;
					if (max < current_score.score * min_or_max) {
						best_square = current_score.square;
					} else {
						return max;
					}
					return current_score.score * min_or_max;
				}, best_score);
				return {
					score: best_score,
					square: best_square
				}
			}
		}
	}
};