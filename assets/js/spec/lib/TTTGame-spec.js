const 	TTTBoardModule = require('../../lib/TTTBoard'),
		TTTGameModule = require('../../lib/TTTGame'),
		TTTBoard = TTTBoardModule.TTTBoard,
		PLAYERX = TTTBoardModule.PLAYERX,
		PLAYERO = TTTBoardModule.PLAYERO,
		DRAW = TTTBoardModule.DRAW,
		TTTGame = TTTGameModule.TTTGame
;

describe("TTTBoard", function () {
	it("it should create new instance of TTTGame class", function () {
		let game = new TTTGame();
		expect(game).toBeDefined();
	});
	it("it should init new board when create new game ", function () {
		let game = new TTTGame();
		expect(game.board).toEqual(jasmine.any(TTTBoard));
	});
	// it("game move should return object with Array in square field", function () {
	// 	let game = new TTTGame(3, [[1,1,1],[1,1,1],[1,1,1]]);
	// 	let move = game.randomMove();
	// 	expect(move.square).toEqual(jasmine.any(Array));
	// });
	it("it should calculate correct move on board with last possible motion", function () {
		let game = new TTTGame(3, [[2,3,2],[3,2,3],[2,3,1]]);
		expect(game.randomMove().square).toEqual([2,2]);
	});
	// it("it should play with random moves", ()=>{
	// 	let game = new TTTGame(3);
	// 	let game_result = game.start();
	// 	expect(game_result).toBeGreaterThan(1);
	// 	expect(game_result).toBeLessThan(5);
	// });
	// it("minimax move should return correct last move for PLAYERX", () => {
     //    let game = new TTTGame(3);
     //    let board = new TTTBoard(3, [[1,2,2],[3,2,3],[3,3,2]]);
     //    expect(game.move(board, PLAYERX)).toEqual({score:1, square:[0,0]});
	// });
	it("minimax move should return correct move with three empty PLAYERX", () => {
		let game = new TTTGame(3);
		let board = new TTTBoard(3, [[3,2,1],[3,2,1],[1,3,2]]);
        //console.log('init board', board.toString());
		let move = game.move(board, PLAYERX);

		//console.log('final move', move);
		expect(move).toEqual({score:0, square:[2,0]});
	});
	it("minimax move should return correct move with three empty PLAYERX (alternate move)", () => {
		let game = new TTTGame(3);
		let board = new TTTBoard(3, [[3,2,1],[3,2,1],[1,3,2]]);
		//console.log('init board', board.toString());
		let move = game.move_alternate(board, PLAYERX);

		//console.log('final move', move);
		expect(move).toEqual({score:0, square:[2,0]});
	});
	it("it should play with random moves", () => {
		let game = new TTTGame(3, [[3,2,1],[3,2,1],[1,3,2]]);
		let game_result = game.start();
		expect(game_result).toBeGreaterThan(1);
		expect(game_result).toBeLessThan(5);
	});
});