const 	TTTBoardModule = require('../../lib/TTTBoard'),
		TTTBoard = TTTBoardModule.TTTBoard
;

describe("TTTBoard", function () {
	it("it should create new instance of TTTBoard class", function () {
		let board = new TTTBoard();
		expect(board).toBeDefined();
	});
	it("it should fill the grid while init with empty values", function () {
		let board = new TTTBoard(3);
		expect(board.grid).toEqual([[1,1,1],[1,1,1],[1,1,1]]);
	});
	it("it should fill the grid while init with params", () => {
		let board = new TTTBoard(3, [[1,2,2],[3,2,2],[1,2,2]]);
		expect(board.grid).toEqual([[1,2,2],[3,2,2],[1,2,2]]);
	});
	it("it should do the correct player's move", () => {
		let board = new TTTBoard(3, [[1,2,2],[3,2,1],[1,2,2]]);
		board.move(1,2,3);
		expect(board.grid).toEqual([[1,2,2],[3,2,3],[1,2,2]]);
	});
	it("it should return correct empty squares", () => {
		let board = new TTTBoard(3, [[1,2,2],[3,2,1],[1,2,2]]);
		expect(board.getEmptySquares()).toEqual([[0,0], [1,2], [2,0]]);
	});
	it("it should return correct winner", () => {
		let board = new TTTBoard(3, [[2,3,2],[3,2,3],[1,3,2]]);
		expect(board.checkWinner()).toBe(2);
	});
	it("it should clone the board", () => {
		let board = new TTTBoard(3, [[2,3,2],[3,2,3],[1,3,2]]),
			clone = board.clone();
		expect(clone.grid).toEqual([[2,3,2],[3,2,3],[1,3,2]]);
	})
});
