"use strict";

// Include gulp
const gulp = require('gulp'),
	jasmine     = require('gulp-jasmine'),
	source      = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	browserify  = require('browserify'),
	sourcemaps = require('gulp-sourcemaps')
;

gulp.task('specs', function () {
	return gulp.src('assets/js/spec/lib/*.js')
			.pipe(jasmine());
});

gulp.task('scripts', function() {
	let b = browserify('./assets/js/app.js', { debug: true });
	return b.bundle()
			.pipe(source('app.js'))
			.pipe(buffer())
			.pipe(sourcemaps.init({
				loadMaps: true
			}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('./public/'));
});

gulp.task('default', ['specs', 'scripts']);